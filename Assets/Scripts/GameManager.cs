﻿using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public float Score = 0f;

    void Awake () {
	    if (Instance != null && Instance != this)
	    {
	        Destroy(this);
	        return;
	    }
	    Instance = this;        
	}

    public event GameStateChanged OnGameStateChanged;
    public delegate void GameStateChanged(GameStates gameState);


    public GameStates GameState
    {
        get { return _gameState; }
        set
        {
            _gameState = value;

            if (OnGameStateChanged != null)
                OnGameStateChanged(_gameState);
        }
    }

    private GameStates _gameState = GameStates.Ready;
    public enum GameStates
    {
        Ready,
        Playing,
        Dead,
        Victory
    }

}
