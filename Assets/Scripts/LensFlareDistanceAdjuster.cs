﻿using System.Collections;
using UnityEngine;

public class LensFlareDistanceAdjuster : MonoBehaviour
{

    public Light LightElement;
    public LensFlare LensFlareElement;
    public float Minimum = 0.2f;
    public float Maximum = 1.5f;
    public float RefreshSpeed = 0.1f;
    public float Multiplier = 1f;

    void Start()
    {
        if (LightElement == null)
            LightElement = GetComponent<Light>();

        if (LensFlareElement == null)
            LensFlareElement = GetComponent<LensFlare>();

        StartCoroutine(SlowUpdate());
    }

    IEnumerator SlowUpdate()
    {
        while (Application.isPlaying)
        {
            yield return new WaitForSeconds(RefreshSpeed);
            var distance = Vector3.Distance(transform.position, Camera.main.transform.position);
            LensFlareElement.brightness = Mathf.Clamp((LightElement.intensity * 2f * Multiplier) / distance, Minimum, Maximum);
        }
    }

}
