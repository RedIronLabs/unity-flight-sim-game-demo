﻿using System;
using System.Collections;
using UnityEngine;

public class RandomDirection : MonoBehaviour {

    public enum Directions
    {
        Left,
        Right,
        Up,
        Down
    }

    private Directions _direction = Directions.Down;

    void Start()
    {
        var rand = (int)UnityEngine.Random.Range(0, 4);        
        if (rand == 0)
            _direction = Directions.Down;
        if (rand == 1)
            _direction = Directions.Left;
        if (rand == 2)
            _direction = Directions.Right;
        if (rand == 3)
            _direction = Directions.Up;

        StartCoroutine(SlowUpdate());
    }

	// Update is called once per frame
	IEnumerator SlowUpdate () {

	    while (Application.isPlaying)
	    {
	        for (var i = 0; i < 60; i++)
	        {
	            var pos = transform.position;
	            switch (_direction)
	            {
	                case Directions.Left:
	                    pos.x -= 0.05f;
	                    break;
	                case Directions.Right:
	                    pos.x += 0.05f;
	                    break;
	                case Directions.Up:
	                    pos.y -= 0.05f;
	                    break;
	                case Directions.Down:
	                    pos.y += 0.05f;
	                    break;
	            }
	            transform.position = pos;
	            yield return new WaitForFixedUpdate();
	        }

	        for (var i = 0; i < 60; i++)
	        {
	            var pos = transform.position;
	            switch (_direction)
	            {
	                case Directions.Left:
	                    pos.x += 0.05f;
	                    break;
	                case Directions.Right:
	                    pos.x -= 0.05f;
	                    break;
	                case Directions.Up:
	                    pos.y += 0.05f;
	                    break;
	                case Directions.Down:
	                    pos.y -= 0.05f;
	                    break;
	            }
	            transform.position = pos;
	            yield return new WaitForFixedUpdate();
	        }
	    }
	}
}
