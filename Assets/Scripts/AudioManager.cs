﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public AudioSource Music;
    public AudioSource Click;

    void Start()
    {
        GameManager.Instance.OnGameStateChanged += OnGameStateChanged;
    }

    void OnDestroy()
    {
        GameManager.Instance.OnGameStateChanged -= OnGameStateChanged;
    }

    void OnGameStateChanged(GameManager.GameStates gamestate)
    {
        switch (gamestate)
        {
            case GameManager.GameStates.Ready:                
                break;

            case GameManager.GameStates.Playing:
                Music.volume = 1f;
                Music.Play();
                break;

            case GameManager.GameStates.Dead:
                Music.Pause();
                break;
        }
    }

    public void PressClick()
    {
        Click.Play();
    }
}
