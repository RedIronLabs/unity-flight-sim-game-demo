﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public Rigidbody ShipBase;
    public Rigidbody ShipWingL;
    public Rigidbody ShipWingR;
    public Collider ShipWingLCollider;
    public Collider ShipWingRCollider;
    public ParticleSystem Motor;
    public GameObject DeathEffect;
    public AudioSource DeathSFX;

    public float ConstantSpeed = 10f;
    private float _currentSpeed = 0f;

    void Start()
    {
        GameManager.Instance.OnGameStateChanged += OnGameStateChanged;
    }

    void OnDestroy()
    {
        GameManager.Instance.OnGameStateChanged -= OnGameStateChanged;
    }

    public Vector3 RotationOffset = new Vector3(-90,180,0);

    void Update()
    {
        if (EnableMouseFollow)
        {
            var tempPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f);
            var mousePosition = Camera.main.ScreenToWorldPoint(tempPosition);

            transform.LookAt(mousePosition, Vector3.up);
            transform.Rotate(RotationOffset);

            var speed = ShipBase.velocity.magnitude;
            if (speed < (ConstantSpeed/3))
                speed = (ConstantSpeed/3);

            ShipBase.velocity = transform.up*speed;
        }
    }

    private bool EnableMouseFollow = false;

    void OnGameStateChanged(GameManager.GameStates gamestate)
    {
        switch (gamestate)
        {
            case GameManager.GameStates.Ready:
                break;

            case GameManager.GameStates.Playing:
                _currentSpeed = ConstantSpeed;
                ShipBase.AddRelativeForce(new Vector3(0f, 1f, 0f) * _currentSpeed, ForceMode.Force);
                EnableMouseFollow = true;
                break;

            case GameManager.GameStates.Dead:
                                
                ShipWingL.isKinematic = false;
                ShipWingL.useGravity = true;
                ShipWingL.transform.parent = null;
                ShipWingLCollider.enabled = true;
                ShipWingL.AddExplosionForce(UnityEngine.Random.Range(10.5f, 15f),Vector3.zero, 2f);

                ShipWingR.isKinematic = false;
                ShipWingR.useGravity = true;
                ShipWingR.transform.parent = null;
                ShipWingRCollider.enabled = true;
                ShipWingR.AddExplosionForce(UnityEngine.Random.Range(10.5f, 15f), Vector3.zero, 2f);

                ShipBase.useGravity = true;

                Motor.Stop();
                DeathEffect.SetActive(true);
                DeathSFX.Play();

                _currentSpeed = 0;
                EnableMouseFollow = false;

                break;

            case GameManager.GameStates.Victory:
                
                Motor.Stop();                
                _currentSpeed = 0;
                EnableMouseFollow = false;
                break;
        }
    }

}
