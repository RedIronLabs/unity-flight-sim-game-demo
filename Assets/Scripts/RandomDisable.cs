﻿using UnityEngine;

public class RandomDisable : MonoBehaviour
{

    public int Odds = 30;
	
	void Start () {
        var rand = (int)UnityEngine.Random.Range(0, 100);

        if (rand < Odds)
            gameObject.SetActive(false);
    }

}
