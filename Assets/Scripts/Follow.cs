﻿using UnityEngine;

public class Follow : MonoBehaviour
{

    public Transform Player;

	// Update is called once per frame
	void LateUpdate ()
	{
	    var pos = transform.position;        
	    pos.z = Player.transform.position.z + 2f;
	    transform.position = pos;
	}
}
