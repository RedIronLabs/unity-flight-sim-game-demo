﻿using System.Collections.Generic;
using UnityEngine;

public class DeathCollider : MonoBehaviour {

    public List<string> DeathTags = new List<string>();

    void OnCollisionEnter(Collision collision)
    {
        if (DeathTags.Contains(collision.gameObject.tag))
            GameManager.Instance.GameState = GameManager.GameStates.Dead;
    }
}
