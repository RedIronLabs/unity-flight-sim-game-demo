﻿using UnityEngine;

public class VictoryCollider : MonoBehaviour
{

    public string VictoryTag = "Victory";

    void OnTriggerEnter(Collider other)
    {
        if (VictoryTag == other.tag)
            GameManager.Instance.GameState = GameManager.GameStates.Victory;
    }
}
