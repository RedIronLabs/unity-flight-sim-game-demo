﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GUIManager : MonoBehaviour
{

    public UnityEngine.UI.Text Score;
    public UnityEngine.UI.Text Victory;
    public UnityEngine.UI.Button Begin;
    public UnityEngine.UI.Button ReStart;

    public AudioManager Audio;

    void Start ()
    {
        StartCoroutine(AddScore());
        GameManager.Instance.OnGameStateChanged += OnGameStateChanged;
    }

    void OnDestroy()
    {
        GameManager.Instance.OnGameStateChanged -= OnGameStateChanged;
    }


    IEnumerator AddScore()
    {
        while (Application.isPlaying)
        {
            yield return new WaitForSeconds(0.25f);
            if (GameManager.Instance.GameState == GameManager.GameStates.Playing)
            {                
                GameManager.Instance.Score += 1f;
                Score.text = GameManager.Instance.Score.ToString();
            }
        }
    }

    void OnGameStateChanged(GameManager.GameStates gamestate)
    {
        switch (gamestate)
        {
            case GameManager.GameStates.Ready:
                Begin.gameObject.SetActive(true);
                break;

            case GameManager.GameStates.Playing:
                ReStart.gameObject.SetActive(false);
                Begin.gameObject.SetActive(false);
                break;

            case GameManager.GameStates.Dead:
                ReStart.gameObject.SetActive(true);
                break;

            case GameManager.GameStates.Victory:
                ReStart.gameObject.SetActive(true);
                Victory.gameObject.SetActive(true);
                break;
        }
    }

    public void StartGame()
    {
        GameManager.Instance.GameState = GameManager.GameStates.Playing;
        Audio.PressClick();
    }
    public void RestartGame()
    {
        GameManager.Instance.Score = 0f;
        SceneManager.LoadScene(0);
        Audio.PressClick();
    }

}
