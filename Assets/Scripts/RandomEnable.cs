﻿using UnityEngine;

public class RandomEnable : MonoBehaviour {

	void Start ()
	{

	    var children = transform.childCount;
        var rand = (int)UnityEngine.Random.Range(0, children);
        transform.GetChild(rand).gameObject.SetActive(true);
    }
}
